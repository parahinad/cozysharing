package android.example.cozysharing;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DialogAddPref extends DialogFragment implements AdapterView.OnItemSelectedListener {
    private TextView p_name, value, weight;
    private Spinner categories, seasons;
    private Button dialog_add_btn;
    private List<Alternative> mCategoriesList;
    private List<Preference> mSeasonsList;
    private String category;
    private String season;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.add_preferences, container, false);
        p_name = (TextView) v.findViewById(R.id.p_name);
        value = (TextView) v.findViewById(R.id.p_value);
        weight = (TextView) v.findViewById(R.id.weight_val);
        categories = (Spinner) v.findViewById(R.id.p_category);
        seasons = (Spinner) v.findViewById(R.id.p_season);
        dialog_add_btn = (Button) v.findViewById(R.id.p_add_btn);
        getData();
        categories.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                category = String.format(Locale.getDefault(), "%d", position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        seasons.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                season = String.format(Locale.getDefault(), "%d", position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        dialog_add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                postPref(category, season, p_name.getText().toString().trim(), value.getText().toString().trim(), UserData.getId(), weight.getText().toString());
            }
        });
        return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void getData() {
        RetroClient.getInstance()
                .getApiService()
                .getCategories()
                .enqueue(new Callback<Alternatives>() {
                    @Override
                    public void onResponse(Call<Alternatives> call, Response<Alternatives> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        mCategoriesList = response.body().getCategories();
                        String[] data = new String[mCategoriesList.size()];
                        int i = 0;
                        for (Alternative category : mCategoriesList) {
                            data[i] = category.getName();
                            i++;
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, data);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        categories.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<Alternatives> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
        RetroClient.getInstance()
                .getApiService()
                .getSeasons()
                .enqueue(new Callback<Preferences>() {
                    @Override
                    public void onResponse(Call<Preferences> call, Response<Preferences> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        mSeasonsList = response.body().getSeasons();
                        String[] data = new String[mSeasonsList.size()];
                        int i = 0;
                        for (Preference season : mSeasonsList) {
                            data[i] = season.getSeason();
                            i++;
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, data);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        seasons.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<Preferences> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private void postPref(String category, String season, String charactiristic, String value, String user, String weight) {
        RetroClient.getInstance()
                .getApiService()
                .postPref(category, season, charactiristic, value, user, weight)
                .enqueue(new Callback<Preference>() {
                    @Override
                    public void onResponse(Call<Preference> call, Response<Preference> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        if (response.body().getMessage().equals("Ok") || response.body().getMessage().equals("Exist")) {
                            getDialog().dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<Preference> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

}
