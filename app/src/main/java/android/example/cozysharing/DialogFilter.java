package android.example.cozysharing;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DialogFilter extends DialogFragment implements AdapterView.OnItemSelectedListener {
    private TextView g_h_value, r_h_value, b_h_value, bth_h_value, g_cw_value, g_c_value, d_c_value, r_cw_value;
    private Spinner trans;
    private Button add_filt;
    private ImageButton g_h_max, g_h_min, r_h_max, r_h_min, b_h_max, b_h_min, bth_h_max, bth_h_min, g_c_max, g_c_min, d_c_max, d_c_min, g_cw_max, g_cw_min, r_cw_max, r_cw_min;
    private View v;
    private String[] cName, value;
    private String transmis, nor, nog, nod, nob, nobth;
    private List<Alternative> mAlternativesList;
    private List<Alternative> mCharList;
    private RecyclerView rv;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (SearchFragment.cat_view == 1) {
            v = inflater.inflate(R.layout.set_filt_cw, container, false);
            g_cw_value = (TextView) v.findViewById(R.id.g_cw_val);
            r_cw_value = (TextView) v.findViewById(R.id.r_cw_val);
            g_cw_max = (ImageButton) v.findViewById(R.id.g_cw_max);
            g_cw_min = (ImageButton) v.findViewById(R.id.g_cw_min);
            r_cw_max = (ImageButton) v.findViewById(R.id.r_cw_max);
            r_cw_min = (ImageButton) v.findViewById(R.id.r_cw_min);
            butIncClick(g_cw_max, g_cw_value);
            butIncClick(r_cw_max, r_cw_value);
            butDecClick(g_cw_min, g_cw_value);
            butDecClick(r_cw_min, r_cw_value);
        } else if (SearchFragment.cat_view == 2) {
            v = inflater.inflate(R.layout.set_filt_c, container, false);
            g_c_value = (TextView) v.findViewById(R.id.g_c_val);
            d_c_value = (TextView) v.findViewById(R.id.d_c_val);
            trans = (Spinner) v.findViewById(R.id.trans);
            g_c_max = (ImageButton) v.findViewById(R.id.g_c_max);
            g_c_min = (ImageButton) v.findViewById(R.id.g_c_min);
            d_c_max = (ImageButton) v.findViewById(R.id.d_c_max);
            d_c_min = (ImageButton) v.findViewById(R.id.d_c_min);
            butDecClick(g_c_min, g_c_value);
            butDecClick(d_c_min, d_c_value);
            butIncClick(g_c_max, g_c_value);
            butIncClick(d_c_max, d_c_value);
            trans.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    transmis = String.format(Locale.getDefault(), "%d", position);
                    getData();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });

            String[] data = {"Manual", "Automatic"};
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text_filt, data);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            trans.setAdapter(adapter);
        } else if (SearchFragment.cat_view == 3) {
            v = inflater.inflate(R.layout.set_filt_h, container, false);
            g_h_value = (TextView) v.findViewById(R.id.g_h_val);
            r_h_value = (TextView) v.findViewById(R.id.r_h_val);
            b_h_value = (TextView) v.findViewById(R.id.b_h_val);
            bth_h_value = (TextView) v.findViewById(R.id.bth_h_val);
            g_h_max = (ImageButton) v.findViewById(R.id.g_h_max);
            g_h_min = (ImageButton) v.findViewById(R.id.g_h_min);
            r_h_max = (ImageButton) v.findViewById(R.id.r_h_max);
            r_h_min = (ImageButton) v.findViewById(R.id.r_h_min);
            b_h_max = (ImageButton) v.findViewById(R.id.b_h_max);
            b_h_min = (ImageButton) v.findViewById(R.id.b_h_min);
            bth_h_max = (ImageButton) v.findViewById(R.id.bth_h_max);
            bth_h_min = (ImageButton) v.findViewById(R.id.bth_h_min);
            butIncClick(g_h_max, g_h_value);
            butIncClick(r_h_max, r_h_value);
            butIncClick(b_h_max, b_h_value);
            butIncClick(bth_h_max, bth_h_value);
            butDecClick(g_h_min, g_h_value);
            butDecClick(r_h_min, r_h_value);
            butDecClick(b_h_min, b_h_value);
            butDecClick(bth_h_min, bth_h_value);
        }



        value = new String[]{"1", "1", "1", "1"};
        add_filt = (Button) v.findViewById(R.id.apply_filt);
        rv = (RecyclerView) SearchFragment.sv.findViewById(R.id.rv);

        add_filt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SearchFragment.cat_view == 3) {
                    cName = new String[]{"Number of guests", "Number of rooms", "Number of beds", "Number of bathrooms"};
                    if (!SearchFragment.city.equals("")) {
                        rv.setAdapter(null);
                        rv.setLayoutManager(null);
                        Context context = v.getContext();
                        LinearLayoutManager llm = new LinearLayoutManager(context);
                        rv.setLayoutManager(llm);
                        searchRes("3", SearchFragment.city, String.format(Locale.getDefault(), "%d", getSeason()), cName, value);
                        searchRes("4", SearchFragment.city, String.format(Locale.getDefault(), "%d", getSeason()), cName, value);
                        searchRes("5", SearchFragment.city, String.format(Locale.getDefault(), "%d", getSeason()), cName, value);
                    } else {
                        Toast.makeText(getActivity(), "Select city", Toast.LENGTH_SHORT).show();
                    }
                } else if (SearchFragment.cat_view == 2) {
                    cName = new String[]{"Number of guests", "Number of doors", "Transmission"};
                    if (!SearchFragment.city.equals("")) {
                        if (value.length != 3) {
                            value = new String[]{"1", "1", "Manual"};
                        }
                        rv.setAdapter(null);
                        rv.setLayoutManager(null);
                        Context context = v.getContext();
                        LinearLayoutManager llm = new LinearLayoutManager(context);
                        rv.setLayoutManager(llm);
                        searchRes("2", SearchFragment.city, String.format(Locale.getDefault(), "%d", getSeason()), cName, value);
                    } else {
                        Toast.makeText(getActivity(), "Select city", Toast.LENGTH_SHORT).show();
                    }
                } else if (SearchFragment.cat_view == 1) {
                    cName = new String[]{"Number of guests", "Number of rooms"};
                    if (!SearchFragment.city.equals("")) {
                        if (value.length != 2) {
                            value = new String[]{"1", "0"};
                        }
                        rv.setAdapter(null);
                        rv.setLayoutManager(null);
                        Context context = v.getContext();
                        LinearLayoutManager llm = new LinearLayoutManager(context);
                        rv.setLayoutManager(llm);
                        searchRes("1", SearchFragment.city, String.format(Locale.getDefault(), "%d", getSeason()), cName, value);
                    } else {
                        Toast.makeText(getActivity(), "Select city", Toast.LENGTH_SHORT).show();
                    }
                }
                Context context = v.getContext();
                LinearLayoutManager llm = new LinearLayoutManager(context);
                rv.setLayoutManager(llm);
                getDialog().dismiss();
            }

        });


        return v;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    private void getData(){
        if(SearchFragment.cat_view == 1){
            value = new String[] {g_cw_value.getText().toString().trim(), r_cw_value.getText().toString().trim()};
        }else if(SearchFragment.cat_view == 2){
            value = new String[] {g_c_value.getText().toString().trim(), d_c_value.getText().toString().trim(), transmis};
        }else if(SearchFragment.cat_view == 3){
            value = new String[] {g_h_value.getText().toString().trim(), r_h_value.getText().toString().trim(), b_h_value.getText().toString().trim(), bth_h_value.getText().toString().trim()};
        }
    }
    private void butIncClick(ImageButton but, final TextView val){

        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int c_v = Integer.parseInt(val.getText().toString());
                c_v ++;
                val.setText(String.format(Locale.getDefault(), "%d", c_v));
                getData();
            }
        });
    }
    private void butDecClick(ImageButton but, final TextView val){

        but.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int c_v = Integer.parseInt(val.getText().toString());
                if(c_v != 1) {
                    c_v--;
                }
                val.setText(String.format(Locale.getDefault(), "%d", c_v));
                getData();
            }
        });
    }

    private List<Housing> housing;
    private int getSeason(){
        Calendar calendar = Calendar.getInstance();
        if(calendar.get(Calendar.MONTH) >= 2 && calendar.get(Calendar.MONTH) <= 4){
            return 2;
        }else if(calendar.get(Calendar.MONTH) >= 5 && calendar.get(Calendar.MONTH) <= 7){
            return 3;
        }else if(calendar.get(Calendar.MONTH) >= 8 && calendar.get(Calendar.MONTH) <= 10){
            return 4;
        } else {
            return 1;
        }
    }
    private void searchRes(String id_cat, String city, String id_seas, final String[] cName, String[] value) {
        housing = new ArrayList<>();
        RetroClient.getInstance()
                .getApiService()
                .getWeight(UserData.getId(), id_cat, city, id_seas, cName, value)
                .enqueue(new Callback<Alternatives>() {
                    @Override
                    public void onResponse(Call<Alternatives> call, Response<Alternatives> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        System.out.println(response.body().getAlternatives());
                        if (response.body().getAlternatives() != null) {
                            mAlternativesList = response.body().getAlternatives();
                            for (Alternative alternative : mAlternativesList) {
                                createAlt(cName, alternative, housing);
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<Alternatives> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private void createAlt(final String[] cName, final Alternative alternative, final List<Housing> housing){
        RetroClient.getInstance()
                .getApiService()
                .getChar(alternative.getId())
                .enqueue(new Callback<Alternatives>() {
                    @Override
                    public void onResponse(Call<Alternatives> call, Response<Alternatives> response) {
                        if(!response.isSuccessful()){
                            System.out.println(response.code());
                            return;
                        }
                        mCharList = response.body().getAlternatives();
                        for(Alternative charact: mCharList) {
                            if(SearchFragment.cat_view == 1) {
                                if (charact.getName().equals(cName[0])){
                                    nog = charact.getValue();
                                } else if (charact.getName().equals(cName[1])) {
                                    nor = charact.getValue();
                                }
                            } else if(SearchFragment.cat_view == 2){
                                if (charact.getName().equals(cName[0])){
                                    nog = charact.getValue();
                                } else if (charact.getName().equals(cName[1])) {
                                    nod = charact.getValue();
                                } else if (charact.getName().equals(cName[2])) {
                                    transmis = charact.getValue();
                                }
                            } else if(SearchFragment.cat_view == 3){
                                if (charact.getName().equals(cName[0])){
                                    nog = charact.getValue();
                                } else if (charact.getName().equals(cName[1])) {
                                    nor = charact.getValue();
                                } else if (charact.getName().equals(cName[2])) {
                                    nob = charact.getValue();
                                }
                                else if (charact.getName().equals(cName[3])) {
                                    nobth = charact.getValue();
                                }
                            }

                        }
                        if(SearchFragment.cat_view == 1) {
                            housing.add(new Housing(alternative.getName(), String.format(Locale.getDefault(), "%d", alternative.getPrice()), alternative.getAddress(), nog, nor));
                        } else if(SearchFragment.cat_view == 2){
                            if(transmis.equals("0")){
                                transmis = "Manual";
                            }else{
                                transmis = "Automatic";
                            }
                            housing.add(new Housing(alternative.getName(), String.format(Locale.getDefault(), "%d", alternative.getPrice()), alternative.getAddress(), nog, nod, transmis));

                        } else if(SearchFragment.cat_view == 3){
                            housing.add(new Housing(alternative.getName(), String.format(Locale.getDefault(), "%d", alternative.getPrice()), alternative.getAddress(), nog, nor, nob, nobth));
                        }
                        RVAdapter adapter = new RVAdapter(housing);
                        rv.setAdapter(adapter);
                    }

                    @Override
                    public void onFailure(Call<Alternatives> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

}