package android.example.cozysharing;

public class UserData {
    private static String id;
    private static String name;
    private static String surname;
    private static String email;
    private static String phone;
    private static String city;
    private static String country;
    private static String lat;
    private static String lng;
    private static String bday;
    private static String access;

    public UserData(String id, String name, String surname, String email, String phone, String city, String country, String lat, String lng, String bday, String access) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phone = phone;
        this.city = city;
        this.country = country;
        this.lat = lat;
        this.lng = lng;
        this.bday = bday;
        this.access = access;
    }

    public static String getId() {
        return id;
    }

    public static void setId(String id) {
        UserData.id = id;
    }

    public static String getName() {
        return name;
    }

    public static void setName(String name) {
        UserData.name = name;
    }

    public static String getSurname() {
        return surname;
    }

    public static void setSurname(String surname) {
        UserData.surname = surname;
    }

    public static String getEmail() {
        return email;
    }

    public static void setEmail(String email) {
        UserData.email = email;
    }

    public static String getPhone() {
        return phone;
    }

    public static void setPhone(String phone) {
        UserData.phone = phone;
    }

    public static String getCity() {
        return city;
    }

    public static void setCity(String city) {
        UserData.city = city;
    }

    public static String getCountry() {
        return country;
    }

    public static void setCountry(String country) {
        UserData.country = country;
    }

    public static String getLat() {
        return lat;
    }

    public static void setLat(String lat) {
        UserData.lat = lat;
    }

    public static String getLng() {
        return lng;
    }

    public static void setLng(String lng) {
        UserData.lng = lng;
    }

    public static String getBday() {
        return bday;
    }

    public static void setBday(String bday) {
        UserData.bday = bday;
    }

    public static String getAccess() {
        return access;
    }

    public static void setAccess(String access) {
        UserData.access = access;
    }
}
