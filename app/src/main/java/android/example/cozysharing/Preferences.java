package android.example.cozysharing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Preferences {
    @SerializedName("seasons")
    @Expose
    private List<Preference> seasons = null;
    @SerializedName("preferences")
    @Expose
    private List<Preference> preferences = null;

    public List<Preference> getSeasons() {
        return seasons;
    }

    public void setSeasons(ArrayList<Preference> seasons) {
        this.seasons = seasons;
    }

    public List<Preference> getPreferences() {
        return preferences;
    }

    public void setPreferences(List<Preference> preferences) {
        this.preferences = preferences;
    }

}
