package android.example.cozysharing;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Sign_in extends AppCompatActivity {
    private EditText email, password;
    private Vibrator vib;
    Animation animShake;
    private TextInputLayout layoutEmail, layoutPass;
    String res;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ImageButton mBackButton = (ImageButton)findViewById(R.id.back_btn);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Sign_in.this, MainActivity.class);
                startActivity(intent);
            }
        });
        ImageButton mNextButton = (ImageButton) findViewById(R.id.next_btn);
        layoutEmail = (TextInputLayout) findViewById(R.id.layout_email);
        layoutPass = (TextInputLayout) findViewById(R.id.layout_pass);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        animShake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);


        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                res = "";
                submitForm();
            }
        });

    }
    private void submitForm() {
        if (!checkEmail()) {
                email.setAnimation(animShake);
                email.startAnimation(animShake);
                vib.vibrate(VibrationEffect.createOneShot(120, VibrationEffect.DEFAULT_AMPLITUDE));
                return;
        }
        if (!checkPass()) {
            if(!res.isEmpty()) {
                password.setAnimation(animShake);
                password.startAnimation(animShake);
                vib.vibrate(VibrationEffect.createOneShot(120, VibrationEffect.DEFAULT_AMPLITUDE));
                return;
            }
        }

        layoutEmail.setErrorEnabled(false);
        layoutPass.setErrorEnabled(false);
        if (res.equals("true")) {
            login(email.getText().toString().trim());
        }

    }

    private boolean checkEmail() {
        String emVal = email.getText().toString().trim();
        if (emVal.isEmpty() || !isValidEmail(emVal)) {
            layoutEmail.setErrorEnabled(true);
            layoutEmail.setError("Enter a valid email.");
            email.setError("Valid input required");
            return false;
        }
        if (res.equals("false")) {
            layoutEmail.setErrorEnabled(true);
            layoutEmail.setError("User with such data does not exist.");
            email.setError("Valid input required");
            return false;
        } else if(res.equals("Exception")){
            layoutEmail.setErrorEnabled(true);
            layoutEmail.setError("Check your Internet connection!");
            email.setError("Something went wrong");
            return false;
        }
        layoutEmail.setErrorEnabled(false);
        return true;
    }
    private void login(String email){
        RetroClient.getInstance()
                .getApiService()
                .loginUser(email)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            Toast.makeText(Sign_in.this, response.code(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        String[] dob = response.body().getDob().split("T");
                        UserData user = new UserData(response.body().getId(), response.body().getName(), response.body().getSurname(), response.body().getEmail(), response.body().getPhone(), response.body().getCity(), response.body().getCountry(), response.body().getLat(), response.body().getLng(), dob[0], response.body().getAccess());
                        Intent intent = new Intent(Sign_in.this, Content.class);
                        startActivity(intent);
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                        res = "Exception";
                    }
                });
    }

    private void checkPassReq(String email, String pass){
        RetroClient.getInstance()
                .getApiService()
                .checkPass(email, pass)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        if(response.body().getMessage().equals("true")) {
                            res = "true";
                        }else if (response.body().getMessage().equals("Error")){
                            res = "Error";
                        }else {
                            res = "false";
                        }
                        submitForm();
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                        res = "Exception";
                    }
                });
    }
    private boolean checkPass(){
        if(!password.getText().toString().trim().matches("^([0-9a-zA-Z!@#$%^&*]{8,})$")){
            layoutPass.setErrorEnabled(true);
            layoutPass.setError("Your password must be at least 8 characters.");
            password.setError("Valid input required");
            return false;
        }
        if(res.isEmpty()){
            checkPassReq(email.getText().toString().trim(), password.getText().toString().trim());
            //tyt bydet zagruzka kadata
            return false;
        }
        if (res.equals("Error")) {
            layoutPass.setErrorEnabled(true);
            layoutPass.setError("Password is incorrect.");
            password.setError("Valid input required");
            return false;
        }
        layoutPass.setErrorEnabled(false);
        return true;
    }
    private static boolean isValidEmail(String email){
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
}
