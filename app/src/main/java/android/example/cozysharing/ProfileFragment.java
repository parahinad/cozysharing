package android.example.cozysharing;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileFragment extends Fragment {
    private TextView name, surname, email, dob, phone, location, wht, spht, sht, aht, wat, spat, sat, aat, whtt, sphtt, shtt, ahtt, wct, spct, sct, act, wcwt, spcwt, scwt, acwt;
    private ImageButton add_btn, logout;
    private List<Alternative> mCategoriesList;
    private List<Preference> mSeasonsList;
    private List<Preference> mPrefList;
    private Integer[] categories;
    private Integer[] seasons;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_profile, container, false);
        name = (TextView)v.findViewById(R.id.uName);
        surname = (TextView)v.findViewById(R.id.uSurname);
        email = (TextView)v.findViewById(R.id.email_text);
        dob = (TextView)v.findViewById(R.id.dob_text);
        phone = (TextView)v.findViewById(R.id.phone_text);
        location = (TextView)v.findViewById(R.id.location_text);

        wht = (TextView)v.findViewById(R.id.winter_house_text);
        spht = (TextView)v.findViewById(R.id.spring_house_text);
        sht = (TextView)v.findViewById(R.id.summer_house_text);
        aht = (TextView)v.findViewById(R.id.autumn_house_text);

        wat = (TextView)v.findViewById(R.id.winter_apart_text);
        spat = (TextView)v.findViewById(R.id.spring_apart_text);
        sat = (TextView)v.findViewById(R.id.summer_apart_text);
        aat = (TextView)v.findViewById(R.id.autumn_apart_text);

        whtt = (TextView)v.findViewById(R.id.winter_hotel_text);
        sphtt = (TextView)v.findViewById(R.id.spring_hotel_text);
        shtt = (TextView)v.findViewById(R.id.summer_hotel_text);
        ahtt = (TextView)v.findViewById(R.id.autumn_hotel_text);

        wct = (TextView)v.findViewById(R.id.winter_car_text);
        spct = (TextView)v.findViewById(R.id.spring_car_text);
        sct = (TextView)v.findViewById(R.id.summer_car_text);
        act = (TextView)v.findViewById(R.id.autumn_car_text);

        wcwt = (TextView)v.findViewById(R.id.winter_coworking_text);
        spcwt = (TextView)v.findViewById(R.id.spring_coworking_text);
        scwt = (TextView)v.findViewById(R.id.summer_coworking_text);
        acwt = (TextView)v.findViewById(R.id.autumn_coworking_text);

        logout = (ImageButton) v.findViewById(R.id.logout);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UserData user = new UserData("", "", "", "", "", "", "", "", "", "", "");
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });
        add_btn = (ImageButton) v.findViewById(R.id.add_pref);
        add_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogAddPref dialog = new DialogAddPref();
                dialog.show(getFragmentManager(), "");
            }
        });
        getCatSeas();
        getData();
        return v;
    }
    private void getData(){
        name.setText(UserData.getName());
        surname.setText(UserData.getSurname());
        email.setText(UserData.getEmail());
        dob.setText(UserData.getBday());
        if(UserData.getPhone() != null) {
            phone.setText(UserData.getPhone());
        }
        if(UserData.getCity() != null || UserData.getCountry() != null) {
            String loc = UserData.getCity() + ", " + UserData.getCountry();
            location.setText(loc);
        }
    }
    private void fillPref(){
        if(categories != null && seasons != null){
            for (int i = 0; i < categories.length; i++){
                for(int j = 0; j < seasons.length; j++){
                    getPref(String.format(Locale.getDefault(), "%d", i), String.format(Locale.getDefault(), "%d", j));
                }
            }
        }
    }

    private void getPref(final String cat, final String seas){
        RetroClient.getInstance()
                .getApiService()
                .getPref(UserData.getId(), cat, seas)
                .enqueue(new Callback<Preferences>() {
                    @Override
                    public void onResponse(Call<Preferences> call, Response<Preferences> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        mPrefList = response.body().getPreferences();
                        if(mPrefList.size() != 0) {
                            switch (cat) {
                                case "3":
                                    switch (seas) {
                                        case "1":
                                            wht.setVisibility(View.VISIBLE);
                                            showText(mPrefList, wht);
                                            break;
                                        case "2":
                                            spht.setVisibility(View.VISIBLE);
                                            showText(mPrefList, spht);
                                            break;
                                        case "3":
                                            sht.setVisibility(View.VISIBLE);
                                            showText(mPrefList, sht);
                                            break;
                                        case "4":
                                            aht.setVisibility(View.VISIBLE);
                                            showText(mPrefList, aht);
                                            break;
                                    }
                                    break;
                                case "4":
                                    switch (seas) {
                                        case "1":
                                            wat.setVisibility(View.VISIBLE);
                                            showText(mPrefList, wat);
                                            break;
                                        case "2":
                                            spat.setVisibility(View.VISIBLE);
                                            showText(mPrefList, spat);
                                            break;
                                        case "3":
                                            sat.setVisibility(View.VISIBLE);
                                            showText(mPrefList, sat);
                                            break;
                                        case "4":
                                            aat.setVisibility(View.VISIBLE);
                                            showText(mPrefList, aat);
                                            break;
                                    }
                                    break;
                                case "5":
                                    switch (seas) {
                                        case "1":
                                            whtt.setVisibility(View.VISIBLE);
                                            showText(mPrefList, whtt);
                                            break;
                                        case "2":
                                            sphtt.setVisibility(View.VISIBLE);
                                            showText(mPrefList, sphtt);
                                            break;
                                        case "3":
                                            shtt.setVisibility(View.VISIBLE);
                                            showText(mPrefList, shtt);
                                            break;
                                        case "4":
                                            ahtt.setVisibility(View.VISIBLE);
                                            showText(mPrefList, ahtt);
                                            break;
                                    }
                                    break;
                                case "2":
                                    switch (seas) {
                                        case "1":
                                            wct.setVisibility(View.VISIBLE);
                                            showText(mPrefList, wct);
                                            break;
                                        case "2":
                                            spct.setVisibility(View.VISIBLE);
                                            showText(mPrefList, spct);
                                            break;
                                        case "3":
                                            sct.setVisibility(View.VISIBLE);
                                            showText(mPrefList, sct);
                                            break;
                                        case "4":
                                            act.setVisibility(View.VISIBLE);
                                            showText(mPrefList, act);
                                            break;
                                    }
                                    break;
                                case "1":
                                    switch (seas) {
                                        case "1":
                                            wcwt.setVisibility(View.VISIBLE);
                                            showText(mPrefList, wcwt);
                                            break;
                                        case "2":
                                            spcwt.setVisibility(View.VISIBLE);
                                            showText(mPrefList, spcwt);
                                            break;
                                        case "3":
                                            scwt.setVisibility(View.VISIBLE);
                                            showText(mPrefList, scwt);
                                            break;
                                        case "4":
                                            acwt.setVisibility(View.VISIBLE);
                                            showText(mPrefList, acwt);
                                            break;
                                    }
                                    break;
                            }
                        }
                    }
                    @Override
                    public void onFailure(Call<Preferences> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }
    private void getCatSeas() {
        RetroClient.getInstance()
                .getApiService()
                .getCategories()
                .enqueue(new Callback<Alternatives>() {
                    @Override
                    public void onResponse(Call<Alternatives> call, Response<Alternatives> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        mCategoriesList = response.body().getCategories();
                        categories = new Integer[mCategoriesList.size()];
                        int i = 0;
                        for (Alternative category : mCategoriesList) {
                            categories[i] = category.getId();
                            i++;
                        }
                        fillPref();
                    }
                    @Override
                    public void onFailure(Call<Alternatives> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
        RetroClient.getInstance()
                .getApiService()
                .getSeasons()
                .enqueue(new Callback<Preferences>() {
                    @Override
                    public void onResponse(Call<Preferences> call, Response<Preferences> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                        mSeasonsList = response.body().getSeasons();
                        seasons = new Integer[mSeasonsList.size()];
                        int i = 0;
                        for (Preference season : mSeasonsList) {
                            seasons[i] = season.getId();
                            i++;
                        }
                        fillPref();
                    }
                    @Override
                    public void onFailure(Call<Preferences> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private void showText( List<Preference> prefList, TextView textView){
        int i = 0;
        for (Preference preference : prefList) {
            String content = "";
            if(i++ == prefList.size() - 1){
                content += preference.getName() + ": " + preference.getValue();
            } else{
                content += preference.getName() + ": " + preference.getValue() + "\n\n";
            }
            textView.append(content);
        }
    }
}
