package android.example.cozysharing;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface ApiService {
    @GET("/alternatives/alternative")
    Call<Alternatives> getAlternatives();
    @FormUrlEncoded
    @POST("/users/postuser")
    Call<User> createUser(
            @Field("name") String name,
            @Field("surname") String surname,
            @Field("email") String email,
            @Field("password") String password,
            @Field("month") String month,
            @Field("day") String day,
            @Field("year") String year
    );
    @FormUrlEncoded
    @POST("/users/checkemail")
    Call<User> checkEmail(
            @Field("email") String email
    );
    @FormUrlEncoded
    @POST("/users/checkpass")
    Call<User> checkPass(
            @Field("email") String email,
            @Field("password") String password
    );
    @FormUrlEncoded
    @POST("/users/login")
    Call<User> loginUser(
            @Field("email") String email
    );
    @GET("/alternatives")
    Call<Alternatives> getCategories();
    @GET("/preferences/seasons")
    Call<Preferences> getSeasons();
    @FormUrlEncoded
    @POST("/preferences/postpref")
    Call<Preference> postPref(
            @Field("category") String category,
            @Field("season") String season,
            @Field("characteristic") String characteristic,
            @Field("value") String value,
            @Field("user") String user,
            @Field("weight") String weight
    );
    @FormUrlEncoded
    @POST("/preferences")
    Call<Preferences> getPref(
            @Field("user") String user,
            @Field("category") String category,
            @Field("season") String season
    );
    @FormUrlEncoded
    @POST("/alternatives/getalt")
    Call<Alternatives> getAlt(
            @Field("id_category") String id_category,
            @Field("city") String city
    );
    @FormUrlEncoded
    @POST("/alternatives/weight")
    Call<Alternatives> getWeight(
            @Field("id_user") String id_user,
            @Field("id_category") String id_category,
            @Field("city") String city,
            @Field("id_seas") String id_seas,
            @Field("cName") String[] cName,
            @Field("value") String[] value
    );
    @FormUrlEncoded
    @POST("/alternatives/getchar")
    Call<Alternatives> getChar(
            @Field("id_alt") int id_alt
    );
}
