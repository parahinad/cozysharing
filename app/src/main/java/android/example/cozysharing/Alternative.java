package android.example.cozysharing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Alternative {

    @SerializedName("Id")
    @Expose
    private int id;
    @SerializedName("Id_category")
    @Expose
    private int idCategory;
    @SerializedName("Id_users")
    @Expose
    private int idUsers;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Price")
    @Expose
    private int price;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("Country")
    @Expose
    private String country;
    @SerializedName("Lat")
    @Expose
    private double lat;
    @SerializedName("Lng")
    @Expose
    private double lng;
    @SerializedName("Address")
    @Expose
    private String address;

    @SerializedName("Id_alternatives")
    @Expose
    private int idAlternatives;

    @SerializedName("Value")
    @Expose
    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public int getIdAlternatives() {
        return idAlternatives;
    }

    public void setIdAlternatives(int idAlternatives) {
        this.idAlternatives = idAlternatives;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdCategory() {
        return idCategory;
    }

    public void setIdCategory(int idCategory) {
        this.idCategory = idCategory;
    }

    public int getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(int idUsers) {
        this.idUsers = idUsers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}

