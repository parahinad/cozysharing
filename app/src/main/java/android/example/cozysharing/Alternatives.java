package android.example.cozysharing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Alternatives {

    @SerializedName("alternatives")
    @Expose
    private List<Alternative> alternatives = null;
    @SerializedName("categories")
    @Expose
    private List<Alternative> categories = null;
    @SerializedName("id")
    @Expose
    private List<Alternative> id = null;

    public List<Alternative> getId() {
        return id;
    }

    public void setId(List<Alternative> id) {
        this.id = id;
    }
    public List<Alternative> getAlternatives() {
        return alternatives;
    }
    public void setAlternatives(ArrayList<Alternative> alternatives) {
        this.alternatives = alternatives;
    }

    public List<Alternative> getCategories() {
        return categories;
    }

    public void setCategories(List<Alternative> categories) {
        this.categories = categories;
    }
}