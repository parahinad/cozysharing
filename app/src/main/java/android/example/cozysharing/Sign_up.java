package android.example.cozysharing;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputLayout;

import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Sign_up extends AppCompatActivity {
    private EditText name, surname, email, password, birthday;
    private DatePickerDialog.OnDateSetListener mSetListener;
    private Vibrator vib;
    Animation animShake;
    private TextInputLayout layoutName, layoutSurname, layoutEmail, layoutPass, layoutBDay;
    String res;
    String[] subStr;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ImageButton mBackButton = (ImageButton) findViewById(R.id.back_btn);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Sign_up.this, MainActivity.class);
                startActivity(intent);
            }
        });
        ImageButton mNextButton = (ImageButton) findViewById(R.id.next_btn);
        layoutName = (TextInputLayout) findViewById(R.id.layout_name);
        layoutSurname = (TextInputLayout) findViewById(R.id.layout_surname);
        layoutEmail = (TextInputLayout) findViewById(R.id.layout_email);
        layoutPass = (TextInputLayout) findViewById(R.id.layout_pass);
        layoutBDay = (TextInputLayout) findViewById(R.id.layout_bd);
        name = (EditText) findViewById(R.id.name);
        surname = (EditText) findViewById(R.id.surname);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        birthday = (EditText) findViewById(R.id.birthday);
        birthday.setInputType(InputType.TYPE_NULL);
        animShake = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake);
        vib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);

        birthday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int year = calendar.get(Calendar.YEAR);
                int month = calendar.get(Calendar.MONTH);
                int day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog dialog = new DatePickerDialog(
                        Sign_up.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mSetListener,
                        year, month, day
                );
                dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int day) {
                month = month + 1;
                String date = month + "/" + day + "/" + year;
                birthday.setText(date);
            }
        };
        String date = birthday.getText().toString();
        System.out.println(date);
        mNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                res = "";
                submitForm();
            }
        });

    }

    private void createUser(String name, String surname, final String email, String password, String month, String day, String year) {

        RetroClient.getInstance()
                .getApiService()
                .createUser(name, surname, email, password, month, day, year)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            Toast.makeText(Sign_up.this, response.code(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if(response.body().getMessage().equals("Ok")) {
                           login(email);
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    private void submitForm() {
        if (!checkName()) {
            name.setAnimation(animShake);
            name.startAnimation(animShake);
            vib.vibrate(VibrationEffect.createOneShot(120, VibrationEffect.DEFAULT_AMPLITUDE));
            return;
        }
        if (!checkSurname()) {
            surname.setAnimation(animShake);
            surname.startAnimation(animShake);
            vib.vibrate(VibrationEffect.createOneShot(120, VibrationEffect.DEFAULT_AMPLITUDE));
            return;
        }
        if (!checkEmail()) {
            if(!res.isEmpty()) {
                email.setAnimation(animShake);
                email.startAnimation(animShake);
                vib.vibrate(VibrationEffect.createOneShot(120, VibrationEffect.DEFAULT_AMPLITUDE));
                return;
            }
        }
        if (!checkPass()) {
            password.setAnimation(animShake);
            password.startAnimation(animShake);
            vib.vibrate(VibrationEffect.createOneShot(120, VibrationEffect.DEFAULT_AMPLITUDE));
            return;
        }

        if (!checkBDay()) {
            birthday.setAnimation(animShake);
            birthday.startAnimation(animShake);
            vib.vibrate(VibrationEffect.createOneShot(120, VibrationEffect.DEFAULT_AMPLITUDE));
            return;
        }

        layoutName.setErrorEnabled(false);
        layoutSurname.setErrorEnabled(false);
        layoutEmail.setErrorEnabled(false);
        layoutPass.setErrorEnabled(false);
        layoutBDay.setErrorEnabled(false);
        if(res.equals("false")){
            createUser(name.getText().toString().trim(), surname.getText().toString().trim(), email.getText().toString().trim(), password.getText().toString().trim(), subStr[0], subStr[1], subStr[2]);
        }

    }

    private boolean checkName() {
        if (!name.getText().toString().trim().matches("^([А-ЯЁ]{1}[а-яё]+)|([A-Z]{1}[a-z]+)$")) {
            layoutName.setErrorEnabled(true);
            layoutName.setError("The name must begin with a capital letter.");
            name.setError("Please use valid characters for your first name.");
            return false;
        }
        layoutName.setErrorEnabled(false);
        return true;
    }

    private boolean checkSurname() {
        if (!surname.getText().toString().trim().matches("^([А-ЯЁ]{1}[а-яё]+)|([A-Z]{1}[a-z]+)$")) {
            layoutSurname.setErrorEnabled(true);
            layoutSurname.setError("The name must begin with a capital letter.");
            surname.setError("Please use valid characters for your second name.");
            return false;
        }
        layoutSurname.setErrorEnabled(false);
        return true;
    }

    private boolean checkEmail() {
        String emVal = email.getText().toString().trim();
        if (emVal.isEmpty() || !isValidEmail(emVal)) {
            layoutEmail.setErrorEnabled(true);
            layoutEmail.setError("Enter a valid email.");
            email.setError("Valid input required");
            return false;
        }
        if(res.isEmpty()){
            existEmail(emVal);
           //tyt bydet zagruzka kadata
            return false;
        }
        else if (res.equals("true")) {
            layoutEmail.setErrorEnabled(true);
            layoutEmail.setError("A user with such email is already exists.");
            email.setError("Valid input required");
            return false;
        } else if(res.equals("Error")){
            layoutEmail.setErrorEnabled(true);
            layoutEmail.setError("Check your Internet connection!");
            email.setError("Something went wrong");
            return false;
        }
        layoutEmail.setErrorEnabled(false);
        return true;
    }

    private void existEmail(String email){
        RetroClient.getInstance()
                .getApiService()
                .checkEmail(email)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            Toast.makeText(Sign_up.this, response.code(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        if(response.body().getMessage().equals("true")) {
                           res = "true";
                        }else{
                            res = "false";
                        }
                        submitForm();
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                        res = "Error";
                    }
                });
    }
    private boolean checkPass(){
        if(!password.getText().toString().trim().matches("^([0-9a-zA-Z!@#$%^&*]{8,})$")){
            layoutPass.setErrorEnabled(true);
            layoutPass.setError("Your password must be at least 8 characters.");
            password.setError("Valid input required");
            return false;
        }
        layoutPass.setErrorEnabled(false);
        return true;
    }
    private boolean checkBDay() {
        String bday = birthday.getText().toString().trim();
        subStr = bday.split("/", 3);
        LocalDate birthdate = LocalDate.of(Integer.parseInt(subStr[2]), Integer.parseInt(subStr[0]), Integer.parseInt(subStr[1]));
        LocalDate now = LocalDate.now();
        if (bday.equals("") || calculateAge(birthdate, now) < 18) {
            layoutBDay.setErrorEnabled(true);
            layoutBDay.setError("You need to be 18 years old.");
            birthday.setError("Enter a valid birth date.");
            return false;
        }
        layoutBDay.setErrorEnabled(false);
        return true;
    }
    private static boolean isValidEmail(String email){
        return !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    private int calculateAge(LocalDate birthDate, LocalDate currentDate) {
        if ((birthDate != null) && (currentDate != null)) {
            return Period.between(birthDate, currentDate).getYears();
        } else {
            return 0;
        }
    }
    private void login(String email){
        RetroClient.getInstance()
                .getApiService()
                .loginUser(email)
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (!response.isSuccessful()) {
                            Toast.makeText(Sign_up.this, response.code(), Toast.LENGTH_SHORT).show();
                            return;
                        }
                        String[] dob = response.body().getDob().split("T");
                        UserData user = new UserData(response.body().getId(), response.body().getName(), response.body().getSurname(), response.body().getEmail(), response.body().getPhone(), response.body().getCity(), response.body().getCountry(), response.body().getLat(), response.body().getLng(), dob[0], response.body().getAccess());
                        Intent intent = new Intent(Sign_up.this, Content.class);
                        startActivity(intent);
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        t.printStackTrace();
                        res = "Exception";
                    }
                });
    }
}