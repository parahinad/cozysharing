package android.example.cozysharing;

import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment {
    static String city = "";
    private EditText scity;
    private ImageButton house, carsharing, coworking, filt_btn;
    static int cat_view = 3;
    static View sv;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        sv = inflater.inflate(R.layout.fragment_search, container, false);

        scity = (EditText) sv.findViewById(R.id.search_city);
        scity.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    switch (keyCode) {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            // hide keyboard
                            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(scity.getWindowToken(),
                                    InputMethodManager.HIDE_NOT_ALWAYS);
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });
        postStandard();
        scity.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                city = "";
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                city = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {
                city = s.toString();
            }
        });
        house = (ImageButton) sv.findViewById(R.id.home_menu);
        carsharing = (ImageButton) sv.findViewById(R.id.car_menu);
        coworking = (ImageButton) sv.findViewById(R.id.coworking_menu);
        filt_btn = (ImageButton) sv.findViewById(R.id.filt_btn);

        house.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cat_view = 3;
                DialogFilter dialog = new DialogFilter();
                dialog.show(getFragmentManager(), "");
                house.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.darker, null));
                carsharing.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.c413E4C, null));
                coworking.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.c413E4C, null));
            }
        });
        carsharing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cat_view = 2;
                DialogFilter dialog = new DialogFilter();
                dialog.show(getFragmentManager(), "");
                carsharing.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.darker, null));
                house.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.c413E4C, null));
                coworking.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.c413E4C, null));
            }
        });
        coworking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cat_view = 1;
                DialogFilter dialog = new DialogFilter();
                dialog.show(getFragmentManager(), "");
                coworking.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.darker, null));
                carsharing.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.c413E4C, null));
                house.setColorFilter(ResourcesCompat.getColor(getResources(), R.color.c413E4C, null));
            }
        });
        filt_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFilter dialog = new DialogFilter();
                dialog.show(getFragmentManager(), "");
            }
        });
        return sv;
    }

    private void postStandard() {
        String[] cat = {"1", "2", "3", "4", "5"};
        String[] seas = {"1", "2", "3", "4"};
        String[] name = {"Number of guests", "Number of rooms", "Transmission", "Number of doors", "Number of beds", "Number of bathrooms"};
        for (int i = 0; i < cat.length; i++) {
            if (cat[i].equals("1")) {
                for (int j = 0; j < seas.length; j++) {
                    for (int a = 0; a < 1; a++) {
                        postSPref(cat[i], seas[j], name[a], "0", UserData.getId(), "5");
                    }
                }
            } else if (cat[i].equals("2")) {
                for (int j = 0; j < seas.length; j++) {
                    for (int a = 0; a < 4; a++) {
                        if (a != 1) {
                            postSPref(cat[i], seas[j], name[a], "0", UserData.getId(), "5");
                        }
                    }
                }
            } else if (cat[i].equals("3") || cat[i].equals("4") || cat[i].equals("5")) {
                for (int j = 0; j < seas.length; j++) {
                    for (int a = 0; a < name.length; a++) {
                        if (a != 2 && a != 3) {
                            postSPref(cat[i], seas[j], name[a], "0", UserData.getId(), "5");
                        }
                    }
                }
            }
        }
    }

    private void postSPref(String category, String season, String charactiristic, String value, String user, String weight) {
        RetroClient.getInstance()
                .getApiService()
                .postPref(category, season, charactiristic, value, user, weight)
                .enqueue(new Callback<Preference>() {
                    @Override
                    public void onResponse(Call<Preference> call, Response<Preference> response) {
                        if (!response.isSuccessful()) {
                            System.out.println(response.code());
                            return;
                        }
                    }

                    @Override
                    public void onFailure(Call<Preference> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

}
