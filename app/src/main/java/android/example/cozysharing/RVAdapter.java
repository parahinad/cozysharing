package android.example.cozysharing;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class RVAdapter extends RecyclerView.Adapter<RVAdapter.HousingViewHolder>{
    private View v;
    @NonNull
    @Override
    public HousingViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        switch (SearchFragment.cat_view){
            case 3:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.housing_card, viewGroup, false);
                break;
            case 2:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.car_card, viewGroup, false);
                break;
            case 1:
                v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cw_card, viewGroup, false);
                break;
        }
        HousingViewHolder pvh = new HousingViewHolder(v);
        return pvh;
    }
    List<Housing> housing;
    RVAdapter(List<Housing> housing){
        this.housing = housing;
    }
    @Override
    public void onBindViewHolder(@NonNull HousingViewHolder holder, int position) {
        HousingViewHolder.name_tv.setText(housing.get(position).name);
        HousingViewHolder.price_tv.setText(housing.get(position).price);
        HousingViewHolder.address_tv.setText(housing.get(position).address);
        HousingViewHolder.nog_tv.setText(housing.get(position).nog);
        switch (SearchFragment.cat_view){
            case 1:
                HousingViewHolder.nor_tv.setText(housing.get(position).nor);
                break;
            case 3:
                HousingViewHolder.nor_tv.setText(housing.get(position).nor);
                HousingViewHolder.nob_tv.setText(housing.get(position).nob);
                HousingViewHolder.nobth_tv.setText(housing.get(position).nobth);
                break;
            case 2:
                HousingViewHolder.nod_tv.setText(housing.get(position).nod);
                HousingViewHolder.trans_tv.setText(housing.get(position).trans);
                break;
        }
//        HousingViewHolder.noph_tv.setText(housing.get(position).noph);
//        HousingViewHolder.main_img.setImageResource(housing.get(position).photoId);

    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return housing.size();
    }

    public static class HousingViewHolder extends RecyclerView.ViewHolder {
        CardView cv;
        static TextView name_tv, price_tv, address_tv, nor_tv, nob_tv, nobth_tv, noph_tv, nog_tv, nod_tv, trans_tv;
        static ImageView main_img;

        HousingViewHolder(View itemView) {
            super(itemView);

            switch (SearchFragment.cat_view){
                case 3:
                    cv = (CardView) itemView.findViewById(R.id.hcv);

                    break;
                case 2:
                    cv = (CardView) itemView.findViewById(R.id.ccv);

                    break;
                case 1:
                    cv = (CardView) itemView.findViewById(R.id.cwcv);

                    break;
            }
            nor_tv = (TextView)itemView.findViewById(R.id.rooms_num);
            nob_tv = (TextView)itemView.findViewById(R.id.beds_num);
            nobth_tv = (TextView)itemView.findViewById(R.id.bt_num);
            nod_tv = (TextView)itemView.findViewById(R.id.doors_num);
            trans_tv = (TextView)itemView.findViewById(R.id.trans_num);
            name_tv = (TextView)itemView.findViewById(R.id.alt_name_text);
            price_tv = (TextView)itemView.findViewById(R.id.price_text);

            address_tv = (TextView)itemView.findViewById(R.id.location_text);
            nog_tv = (TextView)itemView.findViewById(R.id.guests_num);

//            noph_tv = (TextView)itemView.findViewById(R.id.numb_of_ph);
//            main_img = (ImageView)itemView.findViewById(R.id.hoising_main_image);
        }


    }

}
