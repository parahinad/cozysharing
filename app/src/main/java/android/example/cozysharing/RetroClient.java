package android.example.cozysharing;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClient {
    private static RetroClient mInstance;
    private static final String ROOT_URL = "https://salty-harbor-47749.herokuapp.com";
    private Retrofit mRetrofit;

    private RetroClient() {
        mRetrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static RetroClient getInstance() {
        if (mInstance == null) {
            mInstance = new RetroClient();
        }
        return mInstance;
    }

    public ApiService getApiService() {
        return mRetrofit.create(ApiService.class);
    }

}