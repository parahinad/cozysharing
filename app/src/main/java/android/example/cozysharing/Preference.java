package android.example.cozysharing;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Preference {
    @SerializedName("Id")
    @Expose
    private int id;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Season")
    @Expose
    private String season;
    @SerializedName("Value")
    @Expose
    private int value;
    @Expose
    private String message;
    @SerializedName("weight")
    @Expose
    private int weight;
    @SerializedName("id_characteristics")
    @Expose
    private int idCharacteristics;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSeason() {
        return season;
    }

    public void setSeason(String season) {
        this.season = season;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getIdCharacteristics() {
        return idCharacteristics;
    }

    public void setIdCharacteristics(int idCharacteristics) {
        this.idCharacteristics = idCharacteristics;
    }
}
