package android.example.cozysharing;

public class Housing {

    public String name, price, address, nor, nob, nobth, noph, nog, nod, trans;
    int photoId, p_id;

    public Housing(String name, String price, String address, String nog, String nor, String nob, String nobth) {
        this.name = name;
        this.price = price;
        this.address = address;
        this.nog = nog;
        this.nor = nor;
        this.nob = nob;
        this.nobth = nobth;
    }
    public Housing(String name, String price, String address, String nog, String nod, String trans) {
        this.name = name;
        this.price = price;
        this.address = address;
        this.nog = nog;
        this.nod = nod;
        this.trans = trans;
    }
    public Housing(String name, String price, String address, String nog, String nor) {
        this.name = name;
        this.price = price;
        this.address = address;
        this.nog = nog;
        this.nor = nor;
    }

//    public Housing(String name, String price, String address, String nor, String nob, String nobth, String noph, int photoId) {
//        this.name = name;
//        this.price = price;
//        this.address = address;
//        this.nor = nor;
//        this.nob = nob;
//        this.nobth = nobth;
//        this.noph = noph;
//        this.photoId = photoId;
//    }


}
